FROM alpine:edge

RUN printf '#!/bin/sh\necho hello world\n' >/usr/local/bin/hello \
    && chmod +x /usr/local/bin/hello

RUN echo "Hello gitlab 14.10"

CMD /usr/local/bin/hello
